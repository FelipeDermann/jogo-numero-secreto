Link do deploy desse projeto feito pelo Vercel:
https://jogo-numero-secreto-tau.vercel.app/

Um jogo simples que fiz seguindo um curso da Alura que usa reconhecimento de fala.
O objetivo é usar o microfone para falar qual é o número secreto que é aleatório toda vez que a página é recarregada.
Ao falar algum número a página te dá uma dica se o número secreto é menor ou maior do que o número que você adivinha.

Nesse projeto aprendi muito sobre javascript e como fazer uma página inteira e funcional do zero. Aprendi também sobre as APIs de reconhecimento de fala e como importar uma delas para o projeto e usar a informação de input do microfone para criar um joguinho simples mas divertido.
Consigo entender bem melhor sobre como acessar os componentes necessários e como transferir essas informações no usando o javascript.
Entendo um pouco melhor como muitos dos sites e funcionalidades que usamos neles são implementados no código e espero poder fazer outros tipos de experiências interessantes usando outras tecnologias também.
